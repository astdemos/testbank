﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestBank.Common.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public double Amount { get; set; }

        public DateTimeOffset CreatedAt { get; set; } = DateTimeOffset.Now;

        public string AccountId { get; set; }

        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
    }
}
