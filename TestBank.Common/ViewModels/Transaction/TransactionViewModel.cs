﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestBank.Common.ViewModels
{
    public class TransactionViewModel
    {
        public double Amount { get; set; }
        public string Type { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
    }

    public class DepositAmountViewModel
    {
        [Required(ErrorMessage = "Amount is required")]
        [Range(0.1, double.MaxValue, ErrorMessage = "Amount must be greater than 0")]
        public double Amount { get; set; }

    }

    public class WithdrawAmountViewModel
    {
        [Required(ErrorMessage = "Amount is required")]
        [Range(0.1, double.MaxValue, ErrorMessage = "Amount must be greater than 0")]
        public double Amount { get; set; }

    }

    public class TransferAmountViewModel
    {
        [Required(ErrorMessage ="Please select account")]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Amount is required")]
        [Range(0.1, double.MaxValue, ErrorMessage = "Amount must be greater than 0")]
        public double Amount { get; set; }
    }
}
