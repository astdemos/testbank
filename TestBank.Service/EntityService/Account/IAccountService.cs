﻿using System.Linq;
using TestBank.Common.Models;

namespace TestBank.Service
{
    public interface IAccountService
    {
        IQueryable<Account> GetAccounts(AccountDbContext context);
    }
}
