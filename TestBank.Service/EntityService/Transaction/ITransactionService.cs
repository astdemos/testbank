﻿using System.Linq;
using TestBank.Common.Models;

namespace TestBank.Service
{
    public interface ITransactionService
    {
        double GetCurrentBalanace(AccountDbContext context, string accountId);
        Transaction AddTransaction(AccountDbContext context, Transaction transaction);
        IQueryable<Transaction> GetTransactions(AccountDbContext context, string accountId);
    }
}
