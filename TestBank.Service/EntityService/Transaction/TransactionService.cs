﻿using System;
using System.Linq;
using TestBank.Common.Models;

namespace TestBank.Service
{
    public class TransactionService : ITransactionService
    {
        public Transaction AddTransaction(AccountDbContext context, Transaction transaction)
        {
            if (transaction.Amount < 0)
            {
                var currBal = GetCurrentBalanace(context, transaction.AccountId);
                if (currBal < Math.Abs(transaction.Amount)) throw new Exception("Current balance is low");
            }
            context.Transaction.Add(transaction);
            return transaction;
        }

        public double GetCurrentBalanace(AccountDbContext context, string accountId)
        {
            return context.Transaction
                          .Where(x => x.AccountId == accountId)
                          .Sum(x => x.Amount);
        }

        public IQueryable<Transaction> GetTransactions(AccountDbContext context, string accountId)
        {
            return context.Transaction.Where(t => t.AccountId == accountId);
        }
    }
}
