﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TestBank.Web.Extensions
{
    public static class UserInfo
    {

        /// <summary>
        /// Extension method to get user id from ClaimsPrincipal User
        /// </summary>
        /// <param name="user"></param>
        /// <returns>UserID</returns>
        public static string GetUserId(this ClaimsPrincipal user)
        {

            string userId = String.Empty;
            var nameIdentifierClaim = user.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
            if (nameIdentifierClaim != null)
            {
                userId = nameIdentifierClaim.Value;
            }
            return userId;
        }
    }
}
