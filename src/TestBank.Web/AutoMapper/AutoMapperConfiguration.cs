﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestBank.Common.Models;
using TestBank.Common.ViewModels;

namespace TestBank.Web.AutoMapper
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        {

            CreateMap<Transaction, TransactionViewModel>()
                .ForMember(dest => dest.Type, opts => opts.MapFrom(src => src.Amount > 0 ? "Credited" : "Debited"));

            CreateMap<DepositAmountViewModel, Transaction>();
            CreateMap<WithdrawAmountViewModel, Transaction>()
                   .ForMember(dest => dest.Amount, opts => opts.MapFrom(src => src.Amount * -1));

            CreateMap<TransferAmountViewModel, Transaction>();
        }
    }
}
