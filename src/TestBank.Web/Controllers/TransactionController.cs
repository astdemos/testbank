using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestBank.Common.Models;
using TestBank.Service;
using Microsoft.AspNetCore.Identity;
using TestBank.Web.Extensions;
using System.Linq;
using AutoMapper;
using TestBank.Common.ViewModels;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TestBank.Web.Controllers
{
    [Authorize]
    public class TransactionController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly ITransactionService _transactionService;
        private readonly AccountDbContext _context;
        private readonly IMapper _mapper;

        public TransactionController(IAccountService accountService, ITransactionService transactionService, AccountDbContext context, IMapper mapper)
        {
            _accountService = accountService;
            _transactionService = transactionService;
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            string userId = HttpContext.User.GetUserId();
            var transactions = _transactionService.GetTransactions(_context, userId).OrderByDescending(x=>x.CreatedAt).ToList();
            return View(_mapper.Map<List<TransactionViewModel>>(transactions));
        }

        [HttpGet]
        public IActionResult DepositAmount()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DepositAmount(DepositAmountViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var transactionToAdd = _mapper.Map<Transaction>(model);
            transactionToAdd.AccountId = HttpContext.User.GetUserId();

            _transactionService.AddTransaction(_context, transactionToAdd);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult TransferAmount()
        {
            ViewBag.Users = GetAccounts(HttpContext.User.GetUserId());
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TransferAmount(TransferAmountViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Users = GetAccounts(HttpContext.User.GetUserId());
                return View(model);
            }

            if (model.Amount > _transactionService.GetCurrentBalanace(_context, HttpContext.User.GetUserId()))
            {
                ModelState.AddModelError("Amount", "Amount should not be greater than balance");
                return View(model);
            }

            var transectionToDebit = new Transaction
            {
                Amount = model.Amount * -1,
                AccountId = HttpContext.User.GetUserId()
            };
            _transactionService.AddTransaction(_context, transectionToDebit);

            var transactionToCredit = _mapper.Map<Transaction>(model);
            _transactionService.AddTransaction(_context, transactionToCredit);

            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult WithdrawAmount()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult WithdrawAmount(WithdrawAmountViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (model.Amount > _transactionService.GetCurrentBalanace(_context, HttpContext.User.GetUserId()))
            {
                ModelState.AddModelError("Amount", "Amount should not be greater than balance");
                return View(model);
            }

            var transactionToAdd = _mapper.Map<Transaction>(model);
            transactionToAdd.AccountId = HttpContext.User.GetUserId();

            _transactionService.AddTransaction(_context, transactionToAdd);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult GetBalance()
        {
            return Json(_transactionService.GetCurrentBalanace(_context, HttpContext.User.GetUserId()));  
        }


        #region Helpers

        private List<SelectListItem> GetAccounts(string currentUserId)
        {
            List<SelectListItem> userAccounts = new List<SelectListItem>();

            var accounts = _accountService.GetAccounts(_context)
                                          .Where(x => x.Id != currentUserId)
                                          .ToList();

            foreach (var account in accounts)
            {
                userAccounts.Add(new SelectListItem { Text = account.UserName, Value = account.Id });
            }

            return userAccounts;
        }

        #endregion

    }
}