﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestBank.Common.Models;

namespace TestBank.Web
{
    public static class DbInitializer
    {
        public static async Task Seed(IServiceProvider serviceProvider)
        {
            try
            {

                var _userManager = serviceProvider.GetRequiredService<UserManager<Account>>();
                var user1 = await _userManager.FindByNameAsync("user1@testbank.com");

                if (user1 == null)
                {
                    var firstUser = new Account { UserName = "user1@testbank.com", Email = "user1@testbank.com" };
                    await _userManager.CreateAsync(firstUser, "Admin@1234");
                }


                var user2 = await _userManager.FindByNameAsync("user2@testbank.com");

                if (user2 == null)
                {
                    var secondUser = new Account { UserName = "user2@testbank.com", Email = "user2@testbank.com" };
                    await _userManager.CreateAsync(secondUser, "Admin@1234");
                }


            }
            catch (Exception ex)
            {

            }
        }
    }
}
